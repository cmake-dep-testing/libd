
#include <iostream>
#include "d/d.h"

namespace d {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }
}

void d::doThingD(uint indentation) {
  addIndentation(indentation);
  std::cout << "D v1" << std::endl;
};
